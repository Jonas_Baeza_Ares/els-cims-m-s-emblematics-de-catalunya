## Nom del projecte 

Els cims més emblemàtics de Catalunya. 

### Autor 

Arnau Oliveros i Jonas Baeza

### Temàtica 
La pàgina web constarà d'un llistat de cims de Catalunya, el contingut de la web estarà adreçat als excursionistes que busquin realitzar una pujada a un cim.

### Composició de la pàgina web


**1. Pagina principal**

   *   Anomenada Index.html

   * Constarà d'una introducció a la web amb una breu descripció del que 
        l'usuari trobarà dins la mateixa. 
   * Observarem la presència d'enllaços a altres continguts de la web, tals com; Contacte, Cims de Catalunya, Els 5 cims mes alts, Fauna i Flora dels cims...

   * Aquesta pàgina principal està dissenyada amb l'objectiu de captar l'atenció de l'usuari, per fer-ho comptarà amb un mínim d'elements que aportin la informació necessaria per que l'usuari es senti comode. 

**2. Cims de Catalunya**
   *  Anomenada cims_cat.html

   * Constarà d'un llistat de 100 cims ordenats per la seva altura.
   * Per omplir el llistat extraurem la informació de la [següent web](https://www.icgc.cat/Ciutada/Explora-Catalunya/100-cims-mes-emblematics-de-Catalunya/Llista-dels-100-cims-mes-emblematics-de-Catalunya)

   * Cada cim anirà acompanyat dels següents elements
     * Nom
     * Altura
     * Fotografia
     * Comarca + Municipi
     * Breu descripció

   * S'intentarà que la informació presentada sigui com més tècnica millor ja que la web va dirigida a excursionistes i gent de l'entorn de la muntanya que requereixen aquestes dades per el desenvolupament de les seves activitats.

**3. Fauna i flora dels Cims.**
    * Anomenada Fau_flor.html

    * Constarà d'una explicació de la diversitat de fauna i flora que ens podem trobar en els cims de Catalunya.
   * Si s'escau s'explicarà detalladament les característiques de cada element exposat si es dona el cas que aquests siguin d'interès (Per exemple: Una planta que pot presentar beneficis medicinals)

    * S'intentarà que l'explicació sigui com més tècnica millor. 

**4. Recomanacions**
   * Anomenada recomanacions.html

   * Constarà d'una ennumeració de rutes/cims a realitzar segons el teu nivell i experiencia en el mon de la muntanya.

   * Es donaran recomanacions per a cada nivell de experiencia que tingui la persona que consulta el recurs. (Alt, mitjà i baix)

**5. Contacte**
   * Anomanada contacte.html

    * Es creara un formulari que l'usuari tindrà que omplir si desitja contactar amb nosaltres per a cualsevol dubte.
    * El formulari constarà dels següents apartats: Nom, Correu Electrònic, Motiu Consulta i Explicació de la consulta (més detalladament)